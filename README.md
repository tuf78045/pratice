### Details ###

* Course: CIS 3296
* Term: Spring 2019
* Instructor: Michael Wehar
* Teaching Assistant: Rajorshi Biswas

### Getting Started ###

* This repository is for CIS 3296 examples and programming projects.
* Please fork your own copy of this repository and make your copy private.
* Please add dff72324@gmail.com and rajorshi.biswas@temple.edu as collaborators.

